## [2.0.1](https://gitlab.com/to-be-continuous/helm/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([45d46af](https://gitlab.com/to-be-continuous/helm/commit/45d46af8c2c38355aaffc68e98473cd73121d33e))

## [2.0.0](https://gitlab.com/to-be-continuous/helm/compare/1.4.2...2.0.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([7c72363](https://gitlab.com/to-be-continuous/helm/commit/7c72363c466beb98624d64c220bfa7c1445d4586))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.4.2](https://gitlab.com/to-be-continuous/helm/compare/1.4.1...1.4.2) (2021-07-26)

### Bug Fixes

* add an option to avoid var conflict name ([b6e98a7](https://gitlab.com/to-be-continuous/helm/commit/b6e98a7021e964457a0397ff86e20fe4cb12751d))

## [1.4.1](https://gitlab.com/to-be-continuous/helm/compare/1.4.0...1.4.1) (2021-07-07)

### Bug Fixes

* conflict between vault and scoped vars ([f3e89c0](https://gitlab.com/to-be-continuous/helm/commit/f3e89c0070760459487c93a4e88f608974a6e920))

## [1.4.0](https://gitlab.com/to-be-continuous/helm/compare/1.3.0...1.4.0) (2021-06-19)

### Features

* support multi-lines environment variables substitution ([818ca76](https://gitlab.com/to-be-continuous/helm/commit/818ca767484bb6d623102c5905449ddabf0b7335))

## [1.3.0](https://gitlab.com/to-be-continuous/helm/compare/1.2.1...1.3.0) (2021-06-10)

### Features

* move group ([9306f3c](https://gitlab.com/to-be-continuous/helm/commit/9306f3c06f703220935e7737c3f91686087efa26))

## [1.2.1](https://gitlab.com/Orange-OpenSource/tbc/helm/compare/1.2.0...1.2.1) (2021-06-09)

### Bug Fixes

* **publish:** remove variable from rules:exists expression (unsupported) ([47a1246](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/47a1246d811b2ad484f19db95656183ce1de94c7))

## [1.2.0](https://gitlab.com/Orange-OpenSource/tbc/helm/compare/1.1.1...1.2.0) (2021-05-18)

### Features

* add scoped variables support ([abdc875](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/abdc875fe5d820233a16db86a1c35a3d989e9405))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/helm/compare/1.1.0...1.1.1) (2021-05-17)

### Bug Fixes

* wrong cache scope ([033a82e](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/033a82eea5a4a1b846aaee5561a417a3085663e9))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/helm/compare/1.0.0...1.1.0) (2021-05-12)

### Features

* integrate semantic-release info to helm-package job ([d3aaf7c](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/d3aaf7c19212400ac325ee8888af88e061e2e9b9))

## 1.0.0 (2021-05-06)

### Features

* initial release ([293fbec](https://gitlab.com/Orange-OpenSource/tbc/helm/commit/293fbecbd1b9f33bdb321ed1c84ee3a0bdb94648))
